import logger from './logger.js'
import express from 'express'
import { setup, getChannel } from './queue.js'
const app = express()

app.get('/', (req, res) => {
  logger.info('Request recieved')
  getChannel()
    .sendToQueue('queue', Buffer.from(req.query.message),{},(err, ok)=>{
      logger.info('Added')
      res.send({ message: 'Added to queue successfully' })
    })
})

const port = process.env.port || 8080
setup()
  .then(_ => app.listen(port))
