import queue from './config.js'
import logger from './logger.js'
import lib from 'amqplib/callback_api.js'

var connection = null
var channel = null

const options = { credentials: lib.credentials.plain(queue.user, queue.password) }

const setup = () => new Promise((resolve, reject) => {
  lib.connect(`amqp://${queue.host}`, options, function (err, conn) {
    if (err) {
      logger.error('[AMQP]', err.message)
      return reject(err)
    }
    conn.on('error', function (err) {
      if (err.message !== 'Connection closing') {
        logger.error('[AMQP] conn error', err.message)
      }
    })
    logger.info('[AMQP] connected')
    connection = conn
    
    connection.createConfirmChannel(function (err, ch) {
    	ch.assertQueue('queue')
    	if (closeOnErr(err)) return
    	ch.on('error', function (err) {
      	logger.error('[AMQP] channel error', err.message)
    	})
    	ch.on('close', function () {
      	logger.info('[AMQP] channel closed')
    	})

	
    	channel = ch
  		})
    resolve(true)
  })
})


const getChannel = () => {
  return channel
}

const closeOnErr = (err) => {
  if (!err) return false
  logger.error('[AMQP] error', err)
  connection.close()
  return true
}

export { getChannel, setup }
