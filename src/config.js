'use strict'
export default {
  host: process.env.QUEUE_HOST || 'queue',
  user: process.env.QUEUE_USER,
  password: process.env.QUEUE_PASSWORD
}
