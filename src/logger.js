import bunyan from 'bunyan'
import { join } from 'path'
const { createLogger } = bunyan
var log = createLogger({
  name: 'log',

  streams: [
    {
      level: 'debug',
      stream: process.stdout // log INFO and above to stdout
    },
    {
      level: 'error',
      path: join('/../logs/error.log') // log ERROR and above to a file
    }
  ]
})

export default log
