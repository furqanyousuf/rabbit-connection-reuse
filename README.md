# Rabbit-Connection-Reuse
Purpose of this repository is to explain how to utilize RabbitMQ connection and channel in nodejs express application, that will be established once on application startup and will be reused with each route hit.

```
Rabbit-Connection-Reuse
├─ .gitignore
├─ LICENSE
├─ package-lock.json
├─ package.json
├─ README.md
└─ src
   ├─ config.js
   ├─ index.js
   ├─ logger.js
   └─ queue.js

```